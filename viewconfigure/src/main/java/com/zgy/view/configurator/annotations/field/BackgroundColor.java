package com.zgy.view.configurator.annotations.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Field level annotation.
 *
 * Annotates a given view and sets its background color.
 *
 * ObtainView {@link com.zgy.view.configurator.annotations.field.ObtainView} must be called together
 * with this otherwise a null pointer exception will raise.
 *
 * Created by Zabdiel G. Yusoph on 9/24/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface BackgroundColor {
    int color();
}
