package com.zgy.view.configurator.exception;

/**
 * Created by tim on 9/29/15.
 */
public class ViewConfiguratorException extends RuntimeException {

    public ViewConfiguratorException(String e) {
        super(e);
    }
}
