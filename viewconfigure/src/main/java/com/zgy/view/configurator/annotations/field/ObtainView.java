package com.zgy.view.configurator.annotations.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Field level annotation.
 *
 * Annotates a view field that binds the view into it without
 * the need of calling {@link android.app.Activity}'s findViewById() method.
 *
 * Mandatorily called when using other field annotation to modify a view.
 *
 * Created by Zabdiel G. Yusoph on 9/24/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ObtainView {
    int viewId();
}
