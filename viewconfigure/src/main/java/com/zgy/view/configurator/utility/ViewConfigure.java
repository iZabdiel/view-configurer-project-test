package com.zgy.view.configurator.utility;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;

import com.zgy.view.configurator.annotations.field.BackgroundColor;
import com.zgy.view.configurator.annotations.field.ObtainView;
import com.zgy.view.configurator.annotations.field.ViewOverlay;
import com.zgy.view.configurator.annotations.top.ClassLevelConfig;
import com.zgy.view.configurator.annotations.top.DefineContentView;
import com.zgy.view.configurator.exception.ViewConfiguratorException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * Base class that reads and takes the fields and classes via java Reflection
 * that are annotated and operates their corresponding purposes.
 *
 * Created by Zabdiel G. Yusoph on 9/24/15.
 */
public final class ViewConfigure {

    private static Activity mActivitySource;

    /**
     * The only method accessible from this class.
     *
     * @param activity the activity to be used in reflection.
     */
    public static void initiateConfiguration(Activity activity) {

        mActivitySource = activity;

        Class<? extends Activity> clazz = mActivitySource.getClass();
        Log.e("LAYOUT", "____________");

        ViewConfigure.configureViewsOnClassLevel(clazz);
        ViewConfigure.configureViewsOnFieldLevel(clazz);
    }

    /**
     * Processes all the annotation on class level.
     *
     * @param clazz the Class instance of the activity.
     */
    private static void configureViewsOnClassLevel(Class<? extends Activity> clazz) {

        Set classLevelScope = new HashSet<>();

        for (Annotation annotation : clazz.getDeclaredAnnotations()) {
            classLevelScope.add(annotation.annotationType());
        }

        if (classLevelScope.contains(DefineContentView.class)) {

            DefineContentView defineContentView = clazz.getAnnotation(DefineContentView.class);
            mActivitySource.setContentView(defineContentView.layoutId());

            if (classLevelScope.contains(ClassLevelConfig.class)) {

                ClassLevelConfig classLevelConfig = clazz.getAnnotation(ClassLevelConfig.class);
                ClassLevelConfig.ViewOverlay[] viewOverlays = classLevelConfig.viewOverlay();

                ViewGroup decorView =  (ViewGroup) mActivitySource.getWindow().getDecorView().findViewById(android.R.id.content);
                ViewGroup topLevelLayout = (ViewGroup) decorView.getChildAt(0);

                for (ClassLevelConfig.ViewOverlay viewOverlay : viewOverlays) {

                    ViewConfigure.locateChildAndConfigure(topLevelLayout, viewOverlay.viewType(), viewOverlay.overlay());
                }
            } // END if ClassLevelConfig
        }
        else {
            throw new ViewConfiguratorException("View Configuration error. Must define top level content view first.");
        } // END if BindContentView
    }

    /**
     * Recursively locates all the child views of a given type and customizes it based on the
     * given resource id.
     *
     * @param topLevelViewGroup the top level associated with the activity
     * @param childType the type of the View to process
     * @param drawableOverlay the resource id to be used to customize the annotated field
     */
    private static void locateChildAndConfigure(ViewGroup topLevelViewGroup, Class<? extends View> childType, int... drawableOverlay) {

        for (int x = 0; x < topLevelViewGroup.getChildCount(); x++) {

            if (ViewGroup.class.isAssignableFrom(topLevelViewGroup.getChildAt(x).getClass())) {
                locateChildAndConfigure((ViewGroup) topLevelViewGroup.getChildAt(x), childType, drawableOverlay);
            }
            else if (View.class.isAssignableFrom(topLevelViewGroup.getChildAt(x).getClass())) {

                if (topLevelViewGroup.getChildAt(x).getClass().equals(childType)) {

                    // special cases where drawables of a View are defined into two separate
                    // configuration(i.e Switches, SeekBars etc..))
                    if (childType.equals(Switch.class)) {

                        ((Switch) topLevelViewGroup.getChildAt(x)).setThumbDrawable(mActivitySource.getResources().getDrawable(drawableOverlay[0]));
                        ((Switch) topLevelViewGroup.getChildAt(x)).setTrackDrawable(mActivitySource.getResources().getDrawable(drawableOverlay[1]));
                    }
                    else if (childType.equals(SeekBar.class)) {

                        ((SeekBar) topLevelViewGroup.getChildAt(x)).setThumb(mActivitySource.getResources().getDrawable(drawableOverlay[0]));
                        ((SeekBar) topLevelViewGroup.getChildAt(x)).setProgressDrawable(mActivitySource.getResources().getDrawable(drawableOverlay[1]));
                    }
                    else {

                        if (childType.equals(CheckBox.class)) {
                            ((CheckBox) topLevelViewGroup.getChildAt(x)).setButtonDrawable(mActivitySource.getResources().getDrawable(drawableOverlay[0]));
                        }
                        else if (childType.equals(ProgressBar.class)) {
                            ((ProgressBar) topLevelViewGroup.getChildAt(x)).setProgressDrawable(mActivitySource.getResources().getDrawable(drawableOverlay[0]));
                        }
                        else {
                            topLevelViewGroup.getChildAt(x).setBackground(mActivitySource.getResources().getDrawable(drawableOverlay[0]));
                        }
                    }
                } // END if equal to child type
            }// END if assignable from view
        } // END loop
    }

    /**
     * Processes all the annotation on field level.
     *
     * @param clazz the Class instance of the activity.
     */
    private static void configureViewsOnFieldLevel(Class<? extends Activity> clazz) {

        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {

            Set fieldScopeAnnotations = new HashSet<>();
            Annotation[] annotations = field.getDeclaredAnnotations();

            field.setAccessible(true);

            for (Annotation annotation : annotations) {
                fieldScopeAnnotations.add(annotation.annotationType());
            }

            // field type is child of View
            if (View.class.isAssignableFrom(field.getType()) && annotations.length != 0) {

                // field type is NOT YET annotated to be initialized
                if (!fieldScopeAnnotations.contains(ObtainView.class)) {
                    throw new ViewConfiguratorException("Must Obtain View first of field" + field.getType() + " type.");
                }
                else {

                    ObtainView obtainView = field.getAnnotation(ObtainView.class);
                    View view =  mActivitySource.findViewById(obtainView.viewId());

                    if (fieldScopeAnnotations.contains(BackgroundColor.class)) {

                        BackgroundColor backgroundColor = field.getAnnotation(BackgroundColor.class);
                        view.setBackgroundColor(backgroundColor.color());
                    }

                    if (fieldScopeAnnotations.contains(ViewOverlay.class)) {

                        ViewOverlay viewOverlay = field.getAnnotation(ViewOverlay.class);

                        if (view.getClass().equals(SeekBar.class)) {

                            ((SeekBar) view).setThumb(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[0]));
                            ((SeekBar) view).setProgressDrawable(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[1]));
                        }
                        else if (view.getClass().equals(Switch.class)) {

                            ((Switch) view).setThumbDrawable(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[0]));
                            ((Switch) view).setTrackDrawable(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[1]));
                        }
                        else if (view.getClass().equals(CheckBox.class)) {
                            ((CheckBox) view).setButtonDrawable(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[0]));
                        }
                        else if (view.getClass().equals(ProgressBar.class)) {
                            ((ProgressBar) view).setProgressDrawable(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[0]));
                        }
                        else {
                            view.setBackground(mActivitySource.getResources().getDrawable(viewOverlay.drawableId()[0]));
                        }
                    }

                    try {
                        field.set(mActivitySource, view);
                    }
                    catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            } // END if assignable from view and declared annotations size
        }
    }
}