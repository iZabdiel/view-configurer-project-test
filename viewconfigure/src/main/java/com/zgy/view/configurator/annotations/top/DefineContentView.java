package com.zgy.view.configurator.annotations.top;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Class level annotation.
 *
 * Annotation that defines the content view to be used by the Activity without the need
 * of calling {@link android.app.Activity}'s findViewById() method.
 *
 * Created by Zabdiel G. Yusoph on 9/28/15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DefineContentView {
    int layoutId();
}
