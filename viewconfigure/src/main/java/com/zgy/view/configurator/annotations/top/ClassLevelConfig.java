package com.zgy.view.configurator.annotations.top;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Class level annotation.
 *
 * Top level annotation that takes a series of
 * {@link com.zgy.view.configurator.annotations.top.ClassLevelConfig.ViewOverlay} annotations.
 *
 * Created by Zabdiel G. Yusoph on 9/28/15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassLevelConfig {

    ViewOverlay[] viewOverlay();

    /**
     * Annotation that takes the type of view to be customized and a series of
     * overlay id {@link android.graphics.drawable.Drawable}
     *
     * Some views can be customized with a single configuration while some do not
     * (e.g {@link android.widget.Switch}, {@link android.widget.SeekBar}etc..)
     *
     * @return viewType The type of views to be customized on class level
     *
     * @return overlay The resource ids such drawables to be used to customize the views.
     */
    @interface ViewOverlay {

        Class viewType();
        int[] overlay();
    }
}
