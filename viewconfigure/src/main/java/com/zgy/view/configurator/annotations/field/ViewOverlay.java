package com.zgy.view.configurator.annotations.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Field level annotation.
 *
 * Field level equivalent of
 * {@link com.zgy.view.configurator.annotations.top.ClassLevelConfig.ViewOverlay}
 *
 * Takes the resource id to be used to customize an annotated field of type View.
 *
 * Created by Zabdiel G. Yusoph on 9/28/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ViewOverlay {
    int[] drawableId();
}
