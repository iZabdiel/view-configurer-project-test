package edu.customannotationsample.sample_2;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.zgy.view.configurator.annotations.field.ObtainView;
import com.zgy.view.configurator.annotations.field.ViewOverlay;
import com.zgy.view.configurator.annotations.top.DefineContentView;
import com.zgy.view.configurator.annotations.top.ClassLevelConfig;
import com.zgy.view.configurator.utility.ViewConfigure;

import edu.customannotationsample.R;

/**
 * Created by Zabdiel G. Yusoph on 9/24/15.
 */
@DefineContentView(layoutId = R.layout.activity_class_level_configuration)
@ClassLevelConfig(viewOverlay = {

        @ClassLevelConfig.ViewOverlay(viewType = TextView.class, overlay = R.drawable.custom_text_view_bg),
        @ClassLevelConfig.ViewOverlay(viewType = Button.class, overlay = R.drawable.custom_button_selector),
        @ClassLevelConfig.ViewOverlay(viewType = RadioButton.class, overlay = R.drawable.custom_radio_button_selector),
        @ClassLevelConfig.ViewOverlay(viewType = CheckBox.class, overlay = R.drawable.custom_checkbox),
        @ClassLevelConfig.ViewOverlay(viewType = ProgressBar.class, overlay = R.drawable.custom_progressbar),
        @ClassLevelConfig.ViewOverlay(viewType = ToggleButton.class, overlay = R.drawable.custom_toggle_button),
        @ClassLevelConfig.ViewOverlay(viewType = Switch.class, overlay = {R.drawable.switch_selector, R.drawable.switch_background}),
        @ClassLevelConfig.ViewOverlay(viewType = SeekBar.class, overlay = {R.drawable.seek_bar_custom_thumb_class_level, R.drawable.seek_bar_custom_progress})
})
public class ViewConfiguratorTest extends Activity {

    private String modif;
    
    @ObtainView(viewId = R.id.progressBar1)
    private ProgressBar progressBar1;

    @ObtainView(viewId = R.id.progressBar2)
    private ProgressBar progressBar2;

    @ObtainView(viewId = R.id.textView_field_level_label)
    @ViewOverlay(drawableId = R.drawable.custom_text_view_field_level)
    private TextView mTextViewFieldLevelLabel;

    @ObtainView(viewId = R.id.seekBar2)
    @ViewOverlay(drawableId = {R.drawable.seek_bar_custom_thumb_field_level, R.drawable.seek_bar_custom_progress})
    private SeekBar mSeekBarFieldLevel;

    @ObtainView(viewId = R.id.switch2)
    @ViewOverlay(drawableId = {R.drawable.switch_selector_field_level, R.drawable.switch_background})
    private Switch mSwitchFieldLevel;

    @ObtainView(viewId = R.id.progressBar_field)
    @ViewOverlay(drawableId = R.drawable.custom_progressbar_field_level)
    private ProgressBar mProgressBarFieldLevel;

    @ObtainView(viewId = R.id.checkBox_field)
    @ViewOverlay(drawableId = R.drawable.custom_check_box_field)
    private CheckBox mCheckBoxField;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_level_configuration);
        ViewConfigure.initiateConfiguration(this);

        configTestClassLevelProgressBar();
        configTestFieldLevelProgressBar();
    }

    private void configTestClassLevelProgressBar() {

        new Thread(new Runnable() {

            private int progressBarStatus1;
            private int progressBarStatus2;
            private boolean bar1Done;
            private boolean bar2Done;

            public void run() {

                while (true) {

                    if (progressBarStatus1 > 100) {
                        bar1Done = true;
                    }
                    else {
                        progressBarStatus1++;
                    }

                    if (bar1Done) {

                        if (progressBarStatus2 > 100) {
                            bar2Done = true;
                        }
                        else {
                            progressBarStatus2++;
                        }
                    }

                    try {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    ViewConfiguratorTest.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            if (!bar1Done) {
                                progressBar1.setProgress(progressBarStatus1);
                            }

                            if (!bar2Done && bar1Done){
                                progressBar2.setProgress(progressBarStatus2);
                            }
                        }
                    });


                    if (bar1Done && bar2Done) {

                        bar1Done = false;
                        bar2Done = false;
                        progressBarStatus1 = 0;
                        progressBarStatus2 = 0;
                    }
                }
            }
        }).start();
    }

    private void configTestFieldLevelProgressBar() {

        Drawable b;
        new Thread(new Runnable() {

            private int progressBarStatus;

            public void run() {

                while (true) {

                    if (progressBarStatus > 100) {
                        progressBarStatus = 0;
                    }
                    else {
                        progressBarStatus++;
                    }


                    try {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    ViewConfiguratorTest.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            mProgressBarFieldLevel.setProgress(progressBarStatus);
                        }
                    });
                }
            }
        }).start();
    }
}
